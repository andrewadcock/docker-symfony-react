# Docker for Symfony and React

## Description
 A starting point for Docker container with Symfony, React, Nginx, MySQL, Adminer.

## Usage
1. Clone the repository
    1. `git clone https://gitlab.com/andrewadcock/docker-symfony-react`
2. CD in to the directory
    1. `cd docker-symfony-react`
3. Create .env file in the symfony directory
    1. `touch symfony/.env` 
4. Run `docker-compose up`
5. (Optional) add entries to `/etc/hosts`
    1. ```
       172.28.0.6 react.local www.react.local
       127.0.0.1 symfony.local www.symfony.local
       ```
    2. Visit sites at http://react.local:3000 and http://symfony.local:8000

## Issues?
Sometimes `composer install` fails. If you get a warning like: `Warning: require(/var/www/symfony/vendor/autoload.php): failed to open stream: No such file or directory in /var/www/symfony/config/bootstrap.php on line 5` you'll need to run `composer install` manually.

1. `docker exec -it docker-symfony-react_symfony_1 "bash"`
2. `composer install`

